# NinjaMFG #

Web application for organizing rides (Mitfahrgelegenheiten). Done with the Ninja framework. Integrated geonames web service.

Work in progress as the purpose of this project was to test drive Ninja.

![ninjamfg_overview.PNG](https://bitbucket.org/repo/MAB46g/images/2939707932-ninjamfg_overview.PNG)