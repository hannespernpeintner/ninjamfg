package services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import ninja.cache.NinjaCache;
import ninja.jaxy.GET;
import ninja.jaxy.Order;
import ninja.jaxy.Path;
import ninja.lifecycle.Start;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.geonames.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

@Singleton
public class GeoNamesService {

    public static final String CACHE_KEY_CITIES = "cities";

    @Inject
    NinjaCache cache;
    private PropertiesConfiguration config;

    @Start(order = 91)
    public void startService() throws Exception {
        Logger.getGlobal().info("STARTING GEONAME SERVICE");

        loadGeoNamesConfiguration();
        setCredentials();
    }

    public void setCredentials() {
        if(config == null) {
            try {
                loadGeoNamesConfiguration();
            } catch (ConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        WebService.setUserName(config.getString("name", "ninjamfg"));
        WebService.setToken(config.getString("password"));
    }

    public List<Toponym> getCities() throws Exception {
        List<Toponym> cities = (List<Toponym>) cache.get(CACHE_KEY_CITIES);
        if(cities == null) {
            cities = loadCities();
            cache.set(CACHE_KEY_CITIES, loadCities());
        }
        return cities;
    }
    private List<Toponym> loadCities() throws Exception {
        setCredentials();
        ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
        searchCriteria.setCountryCode("DE");
        ToponymSearchResult searchResult = WebService.search(searchCriteria);

        return searchResult.getToponyms();

    }

    public PropertiesConfiguration loadGeoNamesConfiguration() throws ConfigurationException, IOException {

        config = new PropertiesConfiguration("geonames_account.conf");

        return config;
    }

    public static List<String> convert(List<Toponym> cities) {
        List<String> result = new ArrayList<>();
        for (Toponym toponym: cities) {
            result.add(toponym.getName());
        }
        return result;
    }

}
