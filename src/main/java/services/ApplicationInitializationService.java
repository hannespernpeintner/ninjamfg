package services;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import models.Trip;
import ninja.lifecycle.Start;
import org.geonames.Toponym;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Singleton
public class ApplicationInitializationService {
    @Inject
    Provider<EntityManager> entityManagerProvider;

    @Inject
    GeoNamesService geoNameService;

    @Inject
    public ApplicationInitializationService() {
    }

    @Start(order = 90)
    public void startService() {
        Logger.getGlobal().info("STARTING SERVICE");
        addSomeTrips();
    }

    @Transactional
    private void addSomeTrips() {
        EntityManager entityManager = entityManagerProvider.get();

        entityManager.getTransaction().begin();

        List<Toponym> cities = new ArrayList<>();
        try {
            cities.addAll(geoNameService.getCities());
        } catch (Exception e) {
            e.printStackTrace();
        }

        for(int index = 0; index < 10; index++) {
            Trip trip = new Trip(String.valueOf(System.currentTimeMillis() + index * 200));
            trip.addStops(cities);
            entityManager.persist(trip);
        }

        entityManager.getTransaction().commit();
    }
}
