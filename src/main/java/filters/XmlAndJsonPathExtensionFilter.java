package filters;

import ninja.Context;
import ninja.Filter;
import ninja.FilterChain;
import ninja.Result;

public class XmlAndJsonPathExtensionFilter implements Filter {

    @Override
    public Result filter(FilterChain chain, Context context) {

        String path = context.getRequestPath();

        boolean json = path.endsWith(".json");
        boolean xml = path.endsWith(".xml");

        Result result = chain.next(context);
        if(json) {
            return result
                    .supportedContentTypes(
                            Result.APPLICATION_JSON)
                    .fallbackContentType(Result.APPLICATION_JSON);
        } else if(xml){
            return result
                    .supportedContentTypes(
                            Result.APPLICATION_XML)
                    .fallbackContentType(Result.APPLICATION_XML);
        }
        return result
                .supportedContentTypes(
                        Result.TEXT_HTML,
                        Result.APPLICATION_JSON,
                        Result.APPLICATION_XML)
                .fallbackContentType(Result.APPLICATION_JSON);
    }
}
