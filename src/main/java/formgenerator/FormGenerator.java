package formgenerator;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringEscapeUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.*;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

public class FormGenerator {

    static String newLine = "\n";

    private Object object;

    public FormGenerator(Object object){
        this.object = object;
    }

    public String generateForm() throws IllegalAccessException {

        StringBuilder result = new StringBuilder();

        result.append(startForm());

        addFieldInputs(result);

        result.append(endForm());

        return result.toString();
    }

    private void addFieldInputs(StringBuilder result) {
        Iterable<Field> fields = getFieldsUpTo(object.getClass(), Object.class);

        for (Field field : fields) {
            try {
                result.append(InputGenerator.generate(this, field));
            } catch (IllegalAccessException e) {
                Logger.getGlobal().info(String.format("Can't access field %s on %s, so no inputs generated.", field.getName(), object));
            }
        }
    }

    private String startForm() {
        return String.format("<div class=\"panel panel-default\">%s<div class=\"panel-heading\">%s</div>%s" +
                "<div class=\"panel-body\">%s<form id=\"form-for-%s\" class=\"form\">%s",newLine, getSimpleName(), newLine, newLine, getSimpleName(), newLine);
    }

    private String getSimpleName() {
        return object.getClass().getSimpleName();
    }

    private String endForm() {
        return String.format("</form>%s</div>%s</div>", newLine, newLine);
    }

    // http://stackoverflow.com/questions/16966629/what-is-the-difference-between-getfields-getdeclaredfields-in-java-reflection
    public Iterable<Field> getFieldsUpTo(@Nonnull Class<?> startClass,
                                                @Nullable Class<?> exclusiveParent) {

        List<Field> currentClassFields = Lists.newArrayList(startClass.getDeclaredFields());
        Class<?> parentClass = startClass.getSuperclass();

        if (parentClass != null &&
                (exclusiveParent == null || !(parentClass.equals(exclusiveParent)))) {
            List<Field> parentClassFields =
                    (List<Field>) getFieldsUpTo(parentClass, exclusiveParent);
            currentClassFields.addAll(parentClassFields);
        }

        return currentClassFields;
    }
    
    private static class InputGenerator {

        static String newLine = "\n";

        public static String generate(FormGenerator formGenerator, Field field) throws IllegalAccessException {
            Object object = formGenerator.getObject();
            Class<?> type = field.getType();

            String result = "";

            try {
                field.setAccessible(true);
            } catch (SecurityException e) {
                Logger.getGlobal().info(String.format("Field %s can't be accessed, so no input for this field.", field.getName()));
                return result;
            }

            if(type.equals(String.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), (String) field.get(object));
            } else if(type.equals(Boolean.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), (Boolean) field.get(object));
            } else if(type.equals(boolean.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), field.getBoolean(object));
            } else if(type.equals(Integer.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), (Integer) field.get(object));
            } else if(type.equals(int.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), field.getInt(object));
            } else if(type.equals(Float.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), (Float) field.get(object));
            } else if(type.equals(float.class)) {
                result += generate(formGenerator.getFieldName(field.getName()), field.getFloat(object));
            } else if(type.equals(List.class)) {
                result += "<div class=\"well\">" + newLine;
                result += String.format("<div id=\"%s\">%s", formGenerator.getFieldName(field.getName()), newLine);
                result += generate(formGenerator.getFieldName(field.getName()), (List) field.get(object), (ParameterizedType) field.getGenericType());
                result += "</div>" + newLine;
                result += "</div>" + newLine;
            }

            return result;
        }

        static String generate(String fieldName, String value) {
            String classString = "class=\"form-control\"";
            return wrap(String.format("<input id=\"%s\" type=\"text\" value=\"%s\" %s/>", fieldName, value, classString), fieldName);
        }

        static String generate(String fieldName, Boolean value) {
            return generate(fieldName, (boolean) value);
        }

        static String generate(String fieldName, boolean value) {
            String checkedString = value ? "checked=\"checked\"" : "";
            String classString = "";
            return wrap(String.format("<input id=\"%s\" type=\"checkbox\" %s %s/>", fieldName, checkedString, classString), fieldName);
        }

        static String generate(String fieldName, Integer value) {
            return generate(fieldName, (int) value);
        }

        static String generate(String fieldName, int value) {
            String classString = "class=\"form-control\"";
            return wrap(String.format("<input id=\"%s\" type=\"number\" step=\"1\" value=\"%d\" %s/>", fieldName, value, classString), fieldName);
        }

        static String generate(String fieldName, Float value) {
            return generate(fieldName, (float) value);
        }

        static String generate(String fieldName, float value) {
            String classString = "class=\"form-control\"";
            return wrap(String.format(Locale.US, "<input id=\"%s\" type=\"number\" value=\"%.5f\" %s/>", fieldName, value, classString), fieldName);
        }

        static String generate(String fieldName, List value, ParameterizedType type) {
            StringBuilder builder = new StringBuilder();

            int counter = 0;
            for (Object listItem : value) {
                Class<?> componentClass = (Class<?>) type.getActualTypeArguments()[0];
                String listItemFieldName = fieldName + "_" + counter;
                try {
                    Method InputGeneratorMethod = InputGenerator.class.getDeclaredMethod("generate", String.class, componentClass);
                    String generatedFormElements = (String) InputGeneratorMethod.invoke(null, listItemFieldName, listItem);
                    builder.append(generatedFormElements);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                counter++;
            }

            return builder.toString();
        }

        static String getBeforeInput(String fieldName) {
            return String.format("<div class=\"form-group\">%s<label for=\"%s\">%s</label>%s", newLine, fieldName, fieldName, newLine);
        }
        static String getAfternput() {
            return newLine + "</div>" + newLine;
        }

        static String wrap(String inner, String fieldName) {
            return getBeforeInput(fieldName) + inner + getAfternput();
        }
    }

    private String getFieldName(String name) {
        return name;
        //return getSimpleName() + "-" + name;
    }


    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

}
