package models;

import org.geonames.Toponym;
import services.GeoNamesService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Trip {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String name = "";

    @ElementCollection(fetch=FetchType.EAGER)
    private List<String> stops = new ArrayList<>();

    public Trip() {
        this(String.valueOf(System.currentTimeMillis()));
    }
    public Trip(String name) {
        this.setName(String.valueOf(System.currentTimeMillis()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getStops() {
        return stops;
    }

    public void setStops(List<String> stops) {
        this.stops = stops;
    }

    public void addStops(List<Toponym> cities) {
        this.stops.addAll(GeoNamesService.convert(cities));
    }
}
