package argumentextractors;

import models.Trip;
import ninja.Context;
import ninja.params.ArgumentExtractors;
import util.Util;

import java.io.IOException;
import java.util.List;

public class TripExtractor extends ArgumentExtractors.BodyAsExtractor<Trip> {

    public TripExtractor(Class<Trip> bodyType) {
        super(bodyType);
    }

    @Override
    public Trip extract(Context context) {
        Trip trip = null;

        try {
            String body = "";
            while (context.getReader().ready()) {
                body += context.getReader().readLine();
            }

            List<Util.NameValuePair> params = Util.getNameValuePairsFromQuery("UTF-8", body);

            trip = new Trip();
            trip.setId(Long.valueOf(params.get(0).getName()));
            trip.setName(params.get(1).getName());

            trip.getStops().clear();
            for (int x = 2; x < params.size(); x++) {
                trip.getStops().add(params.get(x).getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return trip;
    }

    @Override
    public Class<Trip> getExtractedType() {
        return Trip.class;
    }

    @Override
    public String getFieldName() {
        return "trip";
    }
}
