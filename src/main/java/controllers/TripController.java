package controllers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import controllers.base.RequestPathFilteredController;
import models.Trip;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.Router;
import ninja.cache.NinjaCache;
import ninja.jpa.UnitOfWork;
import ninja.params.Param;
import ninja.params.Params;
import ninja.params.PathParam;
import services.ApplicationInitializationService;
import services.GeoNamesService;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Singleton
public class TripController extends RequestPathFilteredController {
    public static final String CACHE_KEY_ALL_TRIPS = "allTrips";

    @Inject
    Provider<EntityManager> entityManagerProvider;

    @Inject
    ApplicationInitializationService initService;

    @Inject
    NinjaCache cache;

    @Inject
    Router router;


    @UnitOfWork
    public Result index() {
        EntityManager manager = entityManagerProvider.get();

        Collection<Trip> allTrips = cache.get(CACHE_KEY_ALL_TRIPS, List.class);
        if(allTrips == null) {
            allTrips = manager.createQuery("Select a from Trip a", Trip.class).getResultList();
            cache.set(CACHE_KEY_ALL_TRIPS, allTrips, "30mn");
        }

        return Results
                .ok()
                .render("trips", allTrips);
    }

    @UnitOfWork
//    @Transactional
    public Result tripById(Context context, @PathParam("id") Long id) {
        EntityManager manager = entityManagerProvider.get();

        Trip trip = manager.find(Trip.class, id);

        if(trip == null) {
            context.getFlashScope().error("No trip found for id " + id);
            return Results.redirect(router.getReverseRoute(TripController.class, "index"));
        }
        return Results
                .ok()
                .template("views/TripController/trip.ftl.html")
                .render("trip", trip);
    }

    @Transactional
    public Result saveTrip(Context context, @argumentextractors.Trip Trip trip) {
        EntityManager manager = entityManagerProvider.get();

        if(trip == null) {
            trip = new Trip();
            manager.persist(trip);
            context.getFlashScope().success("Added new");
        } else {
            manager.merge(trip);
            context.getFlashScope().success("Saved");
        }

        cache.delete(CACHE_KEY_ALL_TRIPS);

        return Results
                .ok()
                .template("views/TripController/trip.ftl.html")
                .render("trip", trip);
    }
}
