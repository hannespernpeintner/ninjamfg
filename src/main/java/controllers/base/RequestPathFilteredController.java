package controllers.base;


import filters.XmlAndJsonPathExtensionFilter;
import ninja.FilterWith;

@FilterWith(XmlAndJsonPathExtensionFilter.class)
public abstract class RequestPathFilteredController {
}
