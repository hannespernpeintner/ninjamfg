package controllers;

import com.google.inject.Inject;
import controllers.base.RequestPathFilteredController;
import ninja.Result;
import ninja.Results;
import services.GeoNamesService;

public class GeoNamesController extends RequestPathFilteredController{

    @Inject
    GeoNamesService geoNamesService;

    public Result getCityNames() {
        try {
            return Results.ok()
                    .template("views/GeoNamesController/index.ftl.html")
                    .render("cities", GeoNamesService.convert(geoNamesService.getCities()));
        } catch (Exception e) {
            return Results.internalServerError();
        }
    }
}
