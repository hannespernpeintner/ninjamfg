package controllers;

import ninja.NinjaDocTester;
import org.doctester.testbrowser.Request;
import org.doctester.testbrowser.Response;
import org.junit.Test;

import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class GeoNamesControllerTest extends NinjaDocTester {


    String URL_INDEX = "/cities";
    String URL_INDEX_JSON = "/cities.json";

    @Test
    public void testGetIndex() {

        Response response = makeRequest(
                Request.GET().url(
                        testServerUrl().path(URL_INDEX)));

        assertThat(response.payload, containsString("Hamburg"));


    }

    @Test
    public void testGetIndexJson() {

        Response response = makeRequest(
                Request.GET().url(
                        testServerUrl().path(URL_INDEX_JSON)));

        String contentType = response.headers.get("Content-Type");
        Logger.getGlobal().info(String.valueOf(response.headers));
        Logger.getGlobal().info(contentType);
        assertTrue(contentType.contains("application/json"));
        assertThat(response.payload, containsString("Hamburg"));
    }
}
