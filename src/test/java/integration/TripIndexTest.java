package integration;

import ninja.NinjaFluentLeniumTest;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TripIndexTest extends NinjaFluentLeniumTest {

    @Test
    public void testThatClickOnListItemShowsDetailPage() {

        goTo(getServerAddress() + "/trips");

        assertTrue(pageSource().contains("Trips"));
        assertTrue(pageSource().contains("id=\"link_trip_1\""));

        click("#link_trip_1");

        assertTrue(pageSource().contains("form action=\"/trips\""));
    }

    @Test
    public void testThatTripEditWorks() {

        goTo(getServerAddress() + "/trips/1");

        assertTrue(pageSource().contains("Trips"));
        find("[name=stops]").first().fill().with("xxx");
        find(".btn.btn-default").click();

        goTo(getServerAddress() + "/trips/1");
        assertTrue(pageSource().contains("xxx"));
    }

}
