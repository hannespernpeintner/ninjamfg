package services;

import junit.framework.Assert;
import ninja.NinjaTest;
import org.apache.commons.configuration.ConfigurationException;
import org.geonames.Toponym;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class GeoNamesTest extends NinjaTest {


    @Test
    public void serviceLoadsConfiguration() throws ConfigurationException, IOException {
        new GeoNamesService().loadGeoNamesConfiguration();
    }

    @Test
    public void testThatServiceGetsCityData() throws Exception {

        GeoNamesService service = getInjector().getInstance(GeoNamesService.class);
        Assert.assertTrue(contains(service.getCities(), "Hamburg"));

    }

    private static boolean contains(List<Toponym> toponyms, String name) {
        for (Toponym topo : toponyms) {
            if(topo.getName().contains(name)) {
                return true;
            }
        }
        return false;
    }
}
