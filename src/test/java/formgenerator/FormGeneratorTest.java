package formgenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FormGeneratorTest {

    @Test
    public void testThatFormIsGeneratedCorrectly() throws IllegalAccessException {

        class MyClass {
            private String testString = "testStringValue";
            private Boolean testBoolean = true;
            private Boolean testBooleanObject = false;
            private int testInt = 12;
            private Integer testInteger = 14;
            private float testFloat = 12.0f;
            private Float testFloatObject = 14.0f;
            private List<String> testStringList = new ArrayList() {{
                add("a0");
                add("a1");
            }};
            private List<Boolean> testBooleanList = new ArrayList() {{
                add(true);
                add(false);
            }};
        }

        FormGenerator formGenerator = new FormGenerator(new MyClass());

        String result = formGenerator.generateForm();

        String newLine = "\n";
        String expectedForm = "<div class=\"panel panel-default\">" + newLine +
                "<div class=\"panel-heading\">MyClass</div>" + newLine +
                "<div class=\"panel-body\">" + newLine +
                "<form id=\"form-for-MyClass\" class=\"form\">" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testString\">testString</label>" + newLine +
                "<input id=\"testString\" type=\"text\" value=\"testStringValue\" class=\"form-control\"/>" + newLine +
                "</div>" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testBoolean\">testBoolean</label>" + newLine +
                "<input id=\"testBoolean\" type=\"checkbox\" checked=\"checked\" />" + newLine +
                "</div>" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testBooleanObject\">testBooleanObject</label>" + newLine +
                "<input id=\"testBooleanObject\" type=\"checkbox\"  />" + newLine +
                "</div>" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testInt\">testInt</label>" + newLine +
                "<input id=\"testInt\" type=\"number\" step=\"1\" value=\"12\" class=\"form-control\"/>" + newLine +
                "</div>" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testInteger\">testInteger</label>" + newLine +
                "<input id=\"testInteger\" type=\"number\" step=\"1\" value=\"14\" class=\"form-control\"/>" + newLine +
                "</div>" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testFloat\">testFloat</label>" + newLine +
                "<input id=\"testFloat\" type=\"number\" value=\"12.00000\" class=\"form-control\"/>" + newLine +
                "</div>" + newLine +

                "<div class=\"form-group\">" + newLine +
                "<label for=\"testFloatObject\">testFloatObject</label>" + newLine +
                "<input id=\"testFloatObject\" type=\"number\" value=\"14.00000\" class=\"form-control\"/>" + newLine +
                "</div>" + newLine +

                "<div class=\"well\">" + newLine +
                    "<div id=\"testStringList\">" + newLine +
                        "<div class=\"form-group\">" + newLine +
                        "<label for=\"testStringList_0\">testStringList_0</label>" + newLine +
                            "<input id=\"testStringList_0\" type=\"text\" value=\"a0\" class=\"form-control\"/>" + newLine +
                        "</div>" + newLine +
                        "<div class=\"form-group\">" + newLine +
                        "<label for=\"testStringList_1\">testStringList_1</label>" + newLine +
                            "<input id=\"testStringList_1\" type=\"text\" value=\"a1\" class=\"form-control\"/>" + newLine +
                        "</div>" + newLine +
                    "</div>" + newLine +
                "</div>" + newLine +

                "<div class=\"well\">" + newLine +
                    "<div id=\"testBooleanList\">" + newLine +
                        "<div class=\"form-group\">" + newLine +
                            "<label for=\"testBooleanList_0\">testBooleanList_0</label>" + newLine +
                            "<input id=\"testBooleanList_0\" type=\"checkbox\" checked=\"checked\" />" + newLine +
                        "</div>" + newLine +
                        "<div class=\"form-group\">" + newLine +
                        "<label for=\"testBooleanList_1\">testBooleanList_1</label>" + newLine +
                            "<input id=\"testBooleanList_1\" type=\"checkbox\"  />" + newLine +
                        "</div>" + newLine +
                    "</div>" + newLine +
                "</div>" + newLine +

                "</form>" + newLine +
                "</div>" + newLine +
                "</div>";

        assertEquals(expectedForm, result);

    }
}
